package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DemoApplication {

	@GetMapping("/getDetails")
	String home() {
		return "Spring is here edited!";
	}

	public static void main(String[] args) {
		System.out.println("Changes done and committed");
		SpringApplication.run(DemoApplication.class, args);
	}
}